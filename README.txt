- instruction:

Since firewall block the public site and disallow for downloaded contents that required for building Cachethq project. 
This is how to download and keep in the project.

refresh the binary 'cachet_monitor' v3.0 from this original site: 
https://github.com/CastawayLabs/cachet-monitor/releases/download/v3.0/cachet_monitor_linux_amd64

refresh the php script 'installer':
wget https://getcomposer.org/installer

refresh php install.sig file:
wget https://composer.github.io/installer.sig
