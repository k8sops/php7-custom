FROM nginx:1.13.9-alpine

ARG cachet_ver
ARG archive_url

ENV cachet_ver ${cachet_ver:-2.3.18}
ENV archive_url ${archive_url:-https://bitbucket.org/k8sops/cachethq-archive/raw/e4a68347fadd48a45e29b72963249958a312eaae/Cachet-${cachet_ver}.tar.gz}
ENV COMPOSER_VERSION 1.6.3

RUN apk add --no-cache --update \
    postgresql-client \
    postgresql \
    mysql-client \
    php7 \
    php7-redis \
    php7-apcu \
    php7-bcmath \
    php7-dom \
    php7-ctype \
    php7-curl \
    php7-fpm \
    php7-fileinfo \
    php7-gd \
    php7-iconv \
    php7-intl \
    php7-json \
    sqlite \
    php7-mbstring \
    php7-mcrypt \
    php7-mysqlnd \
    php7-opcache \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-pdo_pgsql \
    php7-pdo_sqlite \
    php7-phar \
    php7-posix \
    php7-session \
    php7-sqlite3 \
    php7-simplexml \
    php7-soap \
    php7-xml \
    php7-xmlwriter \
    php7-zip \
    php7-zlib \
    php7-tokenizer \
    wget sqlite git curl bash grep ssmtp mailx \
    supervisor

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -sf /dev/stdout /var/log/php7/error.log && \
    ln -sf /dev/stderr /var/log/php7/error.log

RUN adduser -S -s /bin/bash -u 1001 -G root www-data

RUN touch /var/run/nginx.pid && \
    chown -R www-data:root /var/run/nginx.pid /etc/php7/php-fpm.d

RUN mkdir -p /var/www/html && \
    mkdir -p /usr/share/nginx/cache && \
    mkdir -p /var/cache/nginx && \
    mkdir -p /var/lib/nginx && \
    mkdir -p /etc/nginx/ssl && \
    mkdir -p /etc/cachet && \
    chown -R www-data:root /var/www /usr/share/nginx/cache /var/cache/nginx /var/lib/nginx/ /etc/cachet

# Install composer
RUN wget https://bitbucket.org/k8sops/php7-custom/raw/c2654b2de1225d87d185385e71e44c8371a5dc29/installer -O /tmp/composer-setup.php && \
    wget https://bitbucket.org/k8sops/php7-custom/raw/c2654b2de1225d87d185385e71e44c8371a5dc29/installer.sig -O /tmp/composer-setup.sig && \
    php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" && \
    php /tmp/composer-setup.php --version=$COMPOSER_VERSION --install-dir=bin && \
    php -r "unlink('/tmp/composer-setup.php');"

WORKDIR /var/www/html/
USER 1001

# Install Cachet_HQ
RUN wget ${archive_url} && \
    tar xzf Cachet-${cachet_ver}.tar.gz --strip-components=1 && \
    rm -r Cachet-${cachet_ver}.tar.gz && \
    php /bin/composer.phar global require "hirak/prestissimo:^0.3" && \
    php /bin/composer.phar install -o
